<#include "/templates/xjj-index.ftl">
<#--导航-->
<@nav 'RY_FRAMEWORK','首页公告'/>
<@content id="notice">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Meteor介绍</h3>
        </div>
        <div class="panel-body">
            <h1>使用帮助</h1>
            <h3>第一步：【Meteor权限管理】-【服务器管理】设置账号信息</h3>
            <ul>
                <li>点击【增加】按钮添加服务器信息</li>
                <li>点击【添加人员】，给人员添加该服务器权限</li>
                <li>点击【启动/关闭】，关闭服务器整断权限</li>
                <li>点击【初始化】对服务器进行初始化</li>
                <li>在弹出框中先点击【开启Agent】按钮，等待agent服务器中开启</li>
                <li>然后在【选择应用】 中填入要启动的应用编号，点击【确定应用】，确定要整段的应用</li>
            </ul>

            <h3>第二步：【Meteor】-【服务器列表】选择服务器</h3>
            <ul>
                <li>选择【模块】</li>
                <li>再选择【服务器地址】</li>
                <li>点击【开启】</li>
                <li>等待【arthas执行器】tab页，然后点击查看是否开启成功</li>
            </ul>
            <h3>第三步：【Meteor】-【工作台】进行系统诊断</h3>
            <ul>
                <li>在【类名】中输入需要诊断的类，确定进行查询(支持模糊查询)</li>
                <li>在查询出来的类列表的最前面，点击【方法列表】，展示出方法，选择方法进行诊断</li>
            </ul>
            <h1> 控制台快捷键：</h1>

            <p> Shift+Insert 粘贴</p>

            <p> Ctrl+Insert 复制 </p>

            <h1> 项目地址 </h1>

            <p>readme: <a target="_blank" href="https://gitee.com/reywong/meteor/blob/master/README.md">https://gitee.com/reywong/meteor/blob/master/README.md</a></p>

            <p>gitee: <a target="_blank" href="https://gitee.com/reywong/meteor">https://gitee.com/reywong/meteor</a></p>

            <p>clone: <a target="_blank" href="https://gitee.com/reywong/meteor.git">https://gitee.com/reywong/meteor.git</a></p>

            <h1> 代码测试方案 </h1>

            <p> 请求：http://10.206.34.217:8888/</p>

            <p> 调用的类： com.example.demo.arthas.WelcomeController </p>

            <p> 方法: welcome</p>

            <h1>具体帮助文档</h1>

            <p><a href="https://wiki.uce.cn/pages/viewpage.action?pageId=34247810">Meteor-实践</a></p>

            <p><a href="https://wiki.uce.cn/pages/viewpage.action?pageId=30017007">Meteor-介绍</a></p>

            <p><a href="https://wiki.uce.cn/pages/viewpage.action?pageId=34248085">Meteor-后续规划</a></p>

            <h1> 注意事项 </h1>

            <p> 如果项目启动了 Security manager ,需要在启动脚本中添加，下面启动项</p>

            <p><code> -Djava.security.policy=java.policy</code> </p>

            <p>文件<code>java.policy</code>的代码如下：</p>

            <pre id="javaCode">
                grant {
                    permission org.elasticsearch.ThreadPermission "modifyArbitraryThreadGroup";
                    permission org.elasticsearch.ThreadPermission "modifyArbitraryThread";
                };


                grant {
                    permission java.io.FilePermission "<<ALL FILES>>", "read,write";
                    permission java.util.PropertyPermission "*", "read,write";
                    permission java.lang.RuntimePermission "*";
                    permission java.lang.reflect.ReflectPermission "*";
                    permission java.net.SocketPermission "*", "connect,listen,resolve,accept";
                    permission ognl.OgnlInvokePermission "*";
                 };

            </pre>

        </div>
    </div>
</@content>

<script type="text/javascript">
    // javaEditor = CodeMirror.fromTextArea(document.getElementById("javaCode"), {
    //     lineNumbers: true,
    //     matchBrackets: true,
    //     mode: "text/x-java",
    //     readOnly:'nocursor',
    // });


    initNoticeArthas();

    function initNoticeArthas() {
        var workplatform_ip = '${meteor_param.arthasIp}';
        if (workplatform_ip != '') {
            //显示控制台帮助文档
            var options = {
                id: 'meteor_arthas',
                text: 'arthas执行器',
                url: '${base}/meteor/arthas/index',
                navs: 'meteor,arthas执行器',
                closeable: false,
                show: false
            };
            var tOpions = $.extend({}, XJJ.tabOptions, options);
            XJJ.showTab(tOpions);
        }
    }
</script>
