<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<div id="appinfo">

</div><!-- /.col -->


<script type="text/javascript">
    //显示控制台帮助文档
    $("#appinfo").tabs({
        data: [{
            id: 'meteor_appinfo_help',
            text: '帮助文档',
            url: "${base}/meteor/appinfo/help",
            closeable: false
        }],
        showIndex: 0,
        loadAll: false
    });

    getAppinfo();


    function getAppinfo() {
        var appinfo_ip = '${meteor_param.arthasIp}';
        var appinfo_port = '${meteor_param.arthasPort}';
        var appinfo_agentId = '${meteor_param.arthasAgentId}';
        if (appinfo_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配');
            return;
        }
        if (appinfo_port == '') {
            XJJ.msger('端口号不能为空');
            return;
        }
        if (appinfo_agentId == '') {
            appinfo_agentId = "null";
        }

        var cmds = ["dashboard", "jvm", "sysenv", "sysprop", "mbean", "vmoption", "logger"];

        for (var i = 0; i < cmds.length; i++) {
            var cmd = cmds[i];
            var options = {
                id: 'meteor_appinfo_' + cmd,
                text: cmd,
                url: '${base}/meteor/appinfo/terminal/' + cmd + "/" + appinfo_ip + "/" + appinfo_port + "/" + appinfo_agentId,
                navs: 'meteor,appinfo_' + cmd,
            };
            var tOpions = $.extend({}, XJJ.tabOptions, options);
            $("#appinfo").data("tabs").addTab(tOpions);
        }

        setTimeout(function () {
            $("#XjjTab li a[href='#meteor_appinfo_dashboard']").trigger("click");
        }, 2000);

    }

    //开启连接
    function dashbord(ip, port, agentId, cmd) {
        var path = 'ws://' + ip + ':' + port + '/ws';
        if (agentId != 'null' & agentId != "" && agentId != null) {
            path = path + '?method=connectArthas&id=' + agentId;
        }
        var appinfo_ws;
        try {
            appinfo_ws = new WebSocket(path);
        } catch (e) {
            XJJ.msger("请检查参数是否正确");
        }


        if (appinfo_ws != null) {
            XJJ.msgok("连接成功");
        } else {
            XJJ.msgok("连接失败");
        }

        appinfo_ws.onerror = function () {
            appinfo_ws = null;
            XJJ.msger('连接失败');
        };

        appinfo_ws.onclose = function (message) {
            if (message.code === 2000) {
                XJJ.msgok(message.reason);
            }
        };

        appinfo_ws.onopen = function () {
            var xterm = initXterm('appinfo', true);
            var i = 0;
            appinfo_ws.onmessage = function (event) {
                if (event.type === 'message') {
                    data = event.data;
                    if (data.startsWith("[arthas@") && i == 0) {
                        i = i + 1;
                        data = "";

                        //发送指令
                        if (cmd == 'dashboard') {
                            cmd += " -n 1";
                        }

                        if (cmd == 'block') {
                            cmd = "thread -b";
                        }

                        if (cmd == 'sc') {
                            cmd = "sc *Contorller";
                        }

                        appinfo_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
                        console.log(JSON.stringify({action: 'read', data: cmd + "\r"}));
                    }

                    if (i > 0) {
                        if (data.startsWith("[arthas@")) {
                            disconnect(null, appinfo_ws);
                            return;
                        }

                        if (xterm != null) {
                            xterm.write(data);
                        }
                    }
                }
            };
            var terminalSize = getTerminalSize("appinfo");
            appinfo_ws.send(JSON.stringify({action: 'resize', cols: terminalSize.cols, rows: terminalSize.rows}));

            xterm.open(document.getElementById('appinfo-' + cmd));
        };
        return appinfo_ws;
    }

</script>

