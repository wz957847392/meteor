<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <@querygroup title='主机名'>
            <input type="text" id="websocket_hostname" value="192.168.23.128" class="form-control input-sm"
                   placeholder="请输入机器IP">
        </@querygroup>
        <@querygroup title='用户登录名'>
            <input type="text" id="websocket_username" value="root" class="form-control input-sm" placeholder="请输入用户名">
        </@querygroup>
        <@querygroup title='密码'>
            <input type="password" id="websocket_password" value="reywong" class="form-control input-sm"
                   placeholder="请输入密码">
        </@querygroup>
        <@querygroup title='端口号'>
            <input type="text" id="websocket_port" value="22" class="form-control input-sm" placeholder="请输入端口号">
        </@querygroup>
        <@button type="info" icon="glyphicon glyphicon-plus" onclick="openConnect()">连接</@button>
    </@query>
</@content>
<div class="row">
    <div class="col-xs-12">
        <div id="websocket">

        </div>
    </div><!-- /.col -->
</div><!-- /.row -->


<script type="text/javascript">
    //存储socket 格式 {ws_1:ws,ws_2:ws}
    var wsJson = {};
    //定时清理websocket
    setInterval(function () {
        removeSocket()
    }, 30000);

    //定时清理无效websocket
    function removeSocket() {
        $.each(wsJson, function (key, value) {
            var terminalId = key.replace("ws", "terminal");
            if ($("#" + terminalId).html() == undefined) {
                delete wsJson[key];
                disconnect(null, value);
            }
        });
    }

    //显示控制台帮助文档
    $("#websocket").tabs({
        data: [{
            id: 'websocket-help',
            text: 'SSH控制台使用介绍',
            url: "${base}/meteor/websocket/help",
            closeable: false
        }],
        showIndex: 0,
        loadAll: false
    });

    //开启新的tab页
    function openConnect() {
        var cid = "";
        var hostname = document.getElementById("websocket_hostname").value;
        var username = document.getElementById("websocket_username").value;
        var password = document.getElementById("websocket_password").value;
        if (username == '') {
            XJJ.msger('用户名不能为空');
            return;
        }
        if (password == '') {
            XJJ.msger('密码不能为空');
            return;
        }
        if (hostname == '') {
            XJJ.msger("机器IP不能为空");
            return;
        } else {
            cid = hostname.replace(/\./g, "") + Date.parse(new Date()).toString();
        }
        console.log(cid);
        var options = {
            id: 'meteor_terminal' + cid,
            text: hostname,
            url: '${base}/meteor/websocket/terminal/' + cid,
            navs: 'meteor,terminal' + cid,
        };
        var tOpions = $.extend({}, XJJ.tabOptions, options);
        $("#websocket").data("tabs").addTab(tOpions);
    }

    /**
     * 开启websocket并对接xterm
     * @param xterm
     * @param ws
     * @param terminalId
     * @param cid
     */
    function startconnect(xterm, ws, terminalId, cid) {
        var hostname = document.getElementById("websocket_hostname").value;
        var username = document.getElementById("websocket_username").value;
        var password = document.getElementById("websocket_password").value;
        var port = document.getElementById("websocket_port").value;
        if (hostname == '') {
            XJJ.msger('主机名不能为空');
            return;
        }
        if (username == '') {
            XJJ.msger('用户名不能为空');
            return;
        }
        if (password == '') {
            XJJ.msger('密码不能为空');
            return;
        }
        if (port == '') {
            XJJ.msger('端口号不能为空');
            return;
        }
        if (ws != null) {
            disconnect(xterm, ws);
            XJJ.msger('已经连接！');
        }
        //连接websocket
        var path = 'ws://${host}/websocket/' + cid + '/' + encodeURIComponent(hostname) + '/' + encodeURIComponent(username) + '/' + encodeURIComponent(password) + "/" + encodeURIComponent(port);
        console.log(path);
        ws = new WebSocket(path);
        //添加ws
        wsJson['ws_' + cid] = ws;

        if (ws == null) {
            XJJ.msger("连接失败，请检查账号密码是否正确");
        }

        ws.onerror = function () {
            ws = null;
            XJJ.msger('Connect error');
        };
        ws.onclose = function (message) {
            XJJ.msger("账号或密码错误");
        };
        ws.onopen = function () {
            xterm = initXterm("websocket");
            ws.onmessage = function (event) {
                if (event.type === 'message') {
                    data = event.data;
                    if (xterm != null) {
                        xterm.write(data);
                    }
                }
            };

            xterm.open(document.getElementById(terminalId));
            xterm.on('data', function (data) {
                ws.send(data)
            });
        }
    }

</script>
