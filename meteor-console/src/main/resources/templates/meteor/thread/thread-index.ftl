<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<#include "/templates/xjj-list.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <div class="row">
            <@button type="info"  onclick="sendThreadCmd('thread')">线程列表</@button>
            <@button type="info"  onclick="sendThreadCmd('thread -b')">死锁线程</@button>
            <@querygroup title='查看前N个耗资源线程'>
                <input type="text" id="thread-num" value="10" class="form-control input-sm" placeholder="前N个线程">
            </@querygroup>
            <@button type="info"  onclick="getThreadTopList()">查看耗资源线程</@button>
            <@querygroup title='查看具体线程'>
                <input type="text" id="thread-id" class="form-control input-sm" placeholder="查询具体线程">
            </@querygroup>
            <@button type="info"  onclick="getThreadById()">查看特定线程</@button>
        </div>
    </@query>

</@content>

<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="thread-console-card">
            <div id="thread-console"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var thread_ws = null;

    initThreadConsole();

    function initThreadConsole() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }
        thread_ws = initArthasConsole("thread-console", path, "meteor_thread",true);
    }

    function getThreadTopList() {
        var num = $("#thread-num").val();
        if (num == "") {
            XJJ.msger("请输入要显示的线程数量");
            return;
        }
        var cmd = "thread -n " + num;
        sendThreadCmd(cmd)
    }

    function getThreadById() {
        var id = $("#thread-id").val();
        if (id == "") {
            XJJ.msger("请输入要显示的线程ID");
            return;
        }
        var cmd = "thread " + id;
        sendThreadCmd(cmd)
    }

    function sendThreadCmd(cmd) {
        thread_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));
    }

</script>
