<#--
/****************************************************
 * Description: 服务器信息的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-04 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@input url="${base}/meteor/machineinfo/saveStatus" id=tabId>
    <input type="hidden" name="id" value="${machineinfo.id}"/>
    <@formgroup title='状态'>
        <@swichInForm name="status" val=machineinfo.status onTitle="有效" offTitle="无效"></@swichInForm>
    </@formgroup>
    <@formgroup title='创建时间'>
        <@datetime name="createTime" dateValue=machineinfo.createTime required="required" default=true/>
    </@formgroup>
    <@formgroup title='创建人员ID'>
        <input type="text" name="createPersonId" value="<#if (machineinfo.createPersonId)?? || machineinfo.createPersonId==''>${session_manager_info_key.userId}<#else>${machineinfo.createPersonId}</#if>" check-type="required" readonly="readonly">
    </@formgroup>
    <@formgroup title='操作人员名称'>
        <input type="text" name="createPersonName" value="<#if (machineinfo.createPersonName)?? || machineinfo.createPersonName==''>${session_manager_info_key.userName}<#else>${machineinfo.createPersonName}</#if>" check-type="required" readonly="readonly">
    </@formgroup>
</@input>
