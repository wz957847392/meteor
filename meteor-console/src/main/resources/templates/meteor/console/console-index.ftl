<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <@querygroup title='机器IP'>
            <input type="text" id="console_ip" value="192.168.23.128" class="form-control input-sm"
                   placeholder="请输入机器IP">
        </@querygroup>
        <@querygroup title='端口号'>
            <input type="text" id="console_port" value="8563" class="form-control input-sm" placeholder="请输入机器PORT">
        </@querygroup>
        <@querygroup title='AgentID'>
            <input type="text" id="console_agentId" class="form-control input-sm" placeholder="请输入AgentID">
        </@querygroup>
        <@button type="info" icon="glyphicon glyphicon-plus" onclick="openConnectConsole()">连接</@button>
    </@query>
</@content>
<div id="console">

</div>


<script type="text/javascript">
    //存储socket 格式 {console_ws_1:ws,onsole_ws_2:ws}
    var consoleWsJson = {};
    //定时清理websocket
    setInterval(function () {
        $.each(consoleWsJson, function (key, value) {
            if ($("#" + key).html() == undefined) {
                delete consoleWsJson[key];
                disconnect(null, value);
            }
        });
    }, 10000);

    //显示控制台帮助文档
    $("#console").tabs({
        data: [{
            id: 'console-help',
            text: 'arthas控制台使用介绍',
            url: "${base}/meteor/console/help",
            closeable: false
        }],
        showIndex: 0,
        loadAll: false
    });

    //开启新的tab页
    function openConnectConsole(){
        var cid = "";
        var console_ip = document.getElementById("console_ip").value;
        var console_port = document.getElementById("console_port").value;
        var console_agentId = document.getElementById("console_agentId").value;
        if (console_ip == '') {
            XJJ.msger('IP不能为空');
            return;
        } else if (console_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        } else {
            cid = console_ip.replace(/\./g, "") + Date.parse(new Date()).toString();
        }

        console.log(cid);
        var options = {
            id: 'meteor_console_' + cid,
            text: console_ip,
            url: '${base}/meteor/console/terminal/' + cid,
            navs: 'meteor,console_' + cid
        };
        var tOpions = $.extend({}, XJJ.tabOptions, options);
        $("#console").data("tabs").addTab(tOpions);
    }

    /**
     * 开启websocket并对接xterm
     * @param xterm
     * @param ws
     * @param terminalId
     * @param cid
     */
    function startconnectConsole(xterm, ws, terminalId, cid) {
        var console_ip = document.getElementById("console_ip").value;
        var console_port = document.getElementById("console_port").value;
        var console_agentId = document.getElementById("console_agentId").value;
        if (console_ip == '') {
            XJJ.msger('IP不能为空');
            return;
        }
        if (console_port == '') {
            XJJ.msger('端口号不能为空');
            return;
        }

        if (ws != null) {
            disconnect(xterm, ws);
            XJJ.msger('已经连接！');
        }
        //获取websocket连接
        var path = 'ws://' + console_ip + ':' + console_port + '/ws';
        if (console_agentId != 'null' & console_agentId != "" && console_agentId != null) {
            path = path + '?method=connectArthas&id=' + console_agentId;
        }
        try {
            ws = new WebSocket(path);
            if (ws == null) {
                XJJ.msger("连接失败，请检查账号密码是否正确");
            }
        } catch (e) {
            XJJ.msger("请检查IP或PORT正确");
            return;
        }

        //添加ws
        consoleWsJson['console_ws_' + cid] = ws;

        ws.onerror = function () {
            ws = null;
            XJJ.msger('Connect error');
        };
        ws.onclose = function (message) {
            if (message.code === 2000) {
                XJJ.msger(message.reason);
            }
        };
        ws.onopen = function () {
            xterm = initXterm("console");
            ws.onmessage = function (event) {
                if (event.type === 'message') {
                    data = event.data;
                    if (xterm != null) {
                        xterm.write(data);
                    }
                }
            };

            xterm.open(document.getElementById(terminalId));

            xterm.on('data', function (data) {
                ws.send(JSON.stringify({action: 'read', data: data}))
            });

            window.setInterval(function () {
                if (ws != null) {
                    ws.send(JSON.stringify({action: 'read', data: ""}));
                }
            }, 30000);

        }
    }

</script>

