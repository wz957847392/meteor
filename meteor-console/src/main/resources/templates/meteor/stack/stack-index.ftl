<#--
/****************************************************
 * Description: 菜单的列表页面
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<#include "/templates/xjj-list.ftl">
<@navList navs=navArr/>
<@content>
    <@query>
        <input type="hidden" id="stack-className">
        <input type="hidden" id="stack-method">
        <@querygroup title='监控时长(秒)'>
            <input type="text" id="stack-time" value="10" class="form-control input-sm" placeholder="最大30秒"
                   check-type='required number'>        </@querygroup>
        <@button type="info" onclick="sendStackCmd()">重试</@button>
    </@query>

</@content>

<div class="row">
    <div class="container-fluid px-0">
        <div class="col px-0" id="stack-console-card">
            <div id="stack-console"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var stack_ws = null;

    initStackConsole();

    function initStackConsole() {
        var arthas_ip = '${meteor_param.arthasIp}';
        var arthas_port = '${meteor_param.arthasPort}';
        var arthas_agentId = '${meteor_param.arthasAgentId}';
        if (arthas_ip == '') {
            XJJ.msger('机器IP不能为空,请在[meteor]-[全局参数设置]中进行配置');
            return;
        } else if (arthas_port == '') {
            XJJ.msger('PORT不能为空');
            return;
        }

        //获取websocket连接
        var path = 'ws://' + arthas_ip + ':' + arthas_port + '/ws';
        if (arthas_agentId != 'null' & arthas_agentId != "" && arthas_agentId != null) {
            path = path + '?method=connectArthas&id=' + arthas_agentId;
        }
        stack_ws = initArthasConsole("stack-console", path, "meteor_stack",true);
    }

    function sendStackCmd(className, method) {
        if (className != null) {
            $("#stack-className").val(className);
        }
        if (method != null) {
            $("#stack-method").val(method);
        }

        var className_cmd = $("#stack-className").val();
        var method_cmd = $("#stack-method").val();

        var cmd = "stack " + className_cmd + " " + method_cmd + "";
        stack_ws.send(JSON.stringify({action: 'read', data: cmd + "\r"}));

        //防止数据量过大，影响服务器
        var time = $("#stack-time").val();
        if (time > 30) {
            time = 30;
        }
        setTimeout(function () {
            stack_ws.send(JSON.stringify({action: 'read', data: ""}));
        }, time * 1000);
    }

</script>
