<#--
/****************************************************
 * Description: 数据字典的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl">
<@list id=tabId>
    <thead>
    <tr>
        <th><input type="checkbox" class="bscheckall"></th>
        <th>属组</th>
        <th>属组编码</th>
        <th>编码</th>
        <th>值</th>
        <th>备注</th>
        <th>序号</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list page.items?if_exists as item>
        <tr>
            <td>
                <input type="checkbox" class="bscheck" data="id:${item.id}">
            </td>
            <td>
                ${item.groupName}

            </td>
            <td>
                ${item.groupCode}

            </td>
            <td>
                ${item.code}

            </td>
            <td>
                ${item.name}

            </td>
            <td>
                ${item.detail}

            </td>

            <td>
                ${item.sn}

            </td>

            <td>
                <span class="label <#if item.status=XJJConstants.COMMON_STATUS_VALID>label-info</#if> arrowed-in arrowed-in-right">${XJJDict.getText(item.status)}</span>

            </td>
            <td>
                <@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/sys/dict/input/${item.id}','修改数据字典','${tabId}');">修改</@button>
                <@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/sys/dict/delete/${item.id}','删除数据字典？',false,{id:'${tabId}'});">删除</@button>
            </td>
        </tr>
    </#list>
    </tbody>
</@list>
