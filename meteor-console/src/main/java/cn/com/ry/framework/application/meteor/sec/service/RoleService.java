/****************************************************
 * Description: Service for 角色
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-18 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sec.service;

import cn.com.ry.framework.application.meteor.sec.entity.RoleEntity;
import cn.com.ry.framework.application.meteor.sec.entity.RolePrivilegeEntity;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;

import java.util.List;

public interface RoleService  extends XjjService<RoleEntity> {
	public List<RoleEntity> findListNoUser(Long userId);
	public RoleEntity getByCode(String code);
	public List<RolePrivilegeEntity> findPrivilegeByRole(Long roleId);
	public List<RoleEntity> findListByUserId(Long userId);
}
