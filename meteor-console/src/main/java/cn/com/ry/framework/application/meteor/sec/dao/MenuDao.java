/****************************************************
 * Description: DAO for 菜单
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sec.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sec.entity.MenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MenuDao  extends XjjDAO<MenuEntity> {

	public List<MenuEntity> findMenusByPid(@Param("pid") Long pid);
}

