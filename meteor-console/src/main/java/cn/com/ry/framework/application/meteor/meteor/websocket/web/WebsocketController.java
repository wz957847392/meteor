package cn.com.ry.framework.application.meteor.meteor.websocket.web;

import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.websocket.WebSocketServer;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
@RequestMapping("/meteor/websocket")
public class WebsocketController extends SpringControllerSupport {

    @SecPrivilege(title = "SSH控制台")
    @SecList
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String page = this.getViewPath("index");
        return page;
    }

    @RequestMapping(value = "/help")
    public String help(Model model) {
        String page = this.getViewPath("help");
        return page;
    }

    @RequestMapping(value = "/terminal/{cid}")
    public String terminal(Model model, @PathVariable String cid) {
        String page = this.getViewPath("terminal");
        model.addAttribute("cid", cid);
        return page;
    }

    //推送数据接口
    @ResponseBody
    @RequestMapping("/socket/push/{cid}")
    public XjjJson pushToWeb(@PathVariable String cid, String message) {
        try {
            WebSocketServer.sendInfo(message, cid);
        } catch (IOException e) {
            e.printStackTrace();
            return XjjJson.error(cid + "#" + e.getMessage());
        }
        return XjjJson.success(cid);
    }

}
