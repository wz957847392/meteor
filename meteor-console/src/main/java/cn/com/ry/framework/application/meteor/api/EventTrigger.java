package cn.com.ry.framework.application.meteor.api;


/**
 * 接收监控的触发器
 * [应用]10.205.74.74_wet.tomcat.wet-trace-job.16.idc01 JMX 不可用
 * 告警级别：High
 * 告警时间：2019.11.25 09:58:30
 * 故障主机：10.205.74.74_wet.tomcat.wet-trace-job.16.idc01
 * 监控项：jvm Uptime
 * 1 day, 18:37:44
 * <p>
 * 故障恢复：[应用]挂载点 / 磁盘剩余空间不足 10%
 * 主机：10.205.74.52_wet.tomcat.wet-trace-mq.06.idc01
 * 恢复时间：2019.11.24 19:26:27
 * 当前值：87.33 %
 * 持续时间：4m
 */
public class EventTrigger {

    /**
     * 接收监控消息
     *
     * @param eventDescribe 事件描述
     * @param host          事件主机
     * @param eventTime     事件产生时间
     * @param eventValue    事件值
     * @param effectTime    持续影响时间
     * @param remark        备注
     */
    public void getEvent(String eventDescribe, String host, String eventTime, String eventValue, String effectTime, String remark) {
        //接收消息如RocketMq


    }

}
